## Box Game

This Unity game is supposed to be a re-creation of the mail sorting minigame on Dragon Roost Island in the Legend of Zelda: Wind Waker.

### Instructions

*The sounds are kind of loud, careful!*

**Enter** to start the game when prompted. Use **arrows keys** to move the cursor around, and **space** to confirm your selection. **Esc** will bring up the menu which allows you to reset. On the web version, "Exit" doesn't do anything. 

### Demo

A web version of this game can be found [here](https://quantumhu.com/box).

### Extra Info

The lighting for the game looks very bad because I disabled baking because my computer couldn't handle Unity running at a tolerable speed otherwise.

This game was originally designed to be playable by the computer (like an auto-playing mode) so the code may reflect some unused features.