﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyboardInput : MonoBehaviour {

    private Vector3 origCursorPos;

    public Text txtbox;
    public Text timer;
    public Text guess;
    public Text correctCount;
    public Text wrongCount;

    public Button reset;
    public Button exit;

    public AudioSource correctSound;
    public AudioSource wrongSound;

    public GameObject cursor;
    public GameObject guessbg;

    int box_length = 3;
    int box_lBound = 1;

    int box_height = 2;
    int box_hBound = 1;

    /*
    1: 1 2 3
    2: 1 2 3
    */

    // resettable attributes
    int[] cursorPos = new int[2] {0, 0};
    int[] correctBox = new int[2] {-1, -1};

    int numCorrect = 0, numWrong = 0;

    float totalTime = 30f;

    bool timerRunning = false;

    // ai 
    int counter = 0;

    // [leftInput, rightInput, downInput, upInput, dir, suggDir]

    int gen;

    int[] generateInput() {

        int leftInput = canMoveLeft();
        int rightInput = canMoveRight();
        int downInput = canMoveDown();
        int upInput = canMoveUp();

        int[] dir = calcDistance();

        int suggDir = UnityEngine.Random.Range(1, 5);
        // 1 left, 2 right, 3 down, 4 up
        int tempIndex = findMax(dir);

        if (tempIndex == 0) {
            // first value: left-right
            if (dir[tempIndex] > 0) {
                // right
                suggDir = 2;
            } else {
                // left
                suggDir = 1;
            }

        } else if (tempIndex == 1) {
            // second value: up-down
            if (dir[tempIndex] > 0) {
                // down
                suggDir = 3;
            } else {
                // up
                suggDir = 4;
            }
        }

        return new int[5] {leftInput, rightInput, downInput, upInput, suggDir};

    }

    void doInputGeneration() {

        int[] genp = generateInput();
        string output = "";
        for (int i = 0; i < genp.Length; i++) {
            output = output + genp[i].ToString() + ", ";
        }
        Debug.Log(output);

        if (genp[4] == 1) {
            moveLeft();
        } else if (genp[4] == 2) {
            moveRight();
        } else if (genp[4] == 3) {
            moveDown();
        } else if (genp[4] == 4) {
            moveUp();
        }

    }

    int[] calcDistance() {

        int[] dir = new int[2];

        // negative for left or up, positive for right or down
        dir[0] = correctBox[0] - cursorPos[0];
        dir[1] = correctBox[1] - cursorPos[1];

        return dir;

    }

    void Awake() {

        reset.onClick.AddListener(resetFunc);
        exit.onClick.AddListener(exitFunc);

    }

	// Use this for initialization
	void Start() {

        origCursorPos = cursor.transform.position;

        reset.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);

        cursorPos[0] = box_length - box_lBound;
        cursorPos[1] = box_height;

        txtbox.text = "Press enter to start";
        guess.text = "";

        guessbg.gameObject.SetActive(false);

        gen = 0;

        //InvokeRepeating("doInputGeneration", 1.0f, 0.5f);


	}
	
	// Update is called once per frame
	void Update() {

        if (timerRunning) {

            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                moveLeft();
            } else if (Input.GetKeyDown(KeyCode.RightArrow)) {
                moveRight();
            } else if (Input.GetKeyDown(KeyCode.DownArrow)) {
                moveDown();
            } else if (Input.GetKeyDown(KeyCode.UpArrow)) {
                moveUp();
            } else if (Input.GetKeyDown(KeyCode.Space)) {
                checkSelection();
            }

            totalTime -= Time.deltaTime;

            if (totalTime > 0.0) {
                updateTimer(totalTime);
            } else {
                timer.text = "Time's up";
                timerRunning = false;
            }

        }

        if (Input.GetKeyDown(KeyCode.Return)) {

            if (!timerRunning) {
                txtbox.text = "SORT!";

                guessbg.gameObject.SetActive(true);

                genGuess();

                timerRunning = true;

                updateTimer(totalTime);
            }
            

        } else if (Input.GetKeyDown(KeyCode.Escape)) {

            reset.gameObject.SetActive(!reset.gameObject.activeInHierarchy);
            exit.gameObject.SetActive(!exit.gameObject.activeInHierarchy);

        }

        
		
	}

    int canMoveLeft() {
        if (cursorPos[0] > box_lBound) {
            return 1;
        }
        return 0;
    }

    int canMoveRight() {
        if (cursorPos[0] < box_length) {
            return 1;
        }
        return 0;
    }

    int canMoveDown() {
        if (cursorPos[1] < box_height) {
            return 1;
        }
        return 0;
    }

    int canMoveUp() {
        if (cursorPos[1] > box_hBound) {
            return 1;
        }
        return 0;
    }

    void moveLeft() {

        if (canMoveLeft() == 1) {
            transform.Translate(Vector3.left * 3);
            cursorPos[0]--;
        }

    }

    void moveRight() {

        if (canMoveRight() == 1) {
            transform.Translate(Vector3.right * 3);
            cursorPos[0]++;
        }

    }

    void moveDown() {

        if (canMoveDown() == 1) {
            transform.Translate(Vector3.down * 3);
            cursorPos[1]++;
        }

    }

    void moveUp() {

        if (canMoveUp() == 1) {
            transform.Translate(Vector3.up * 3);
            cursorPos[1]--;
        }

    }

    void checkSelection() {

        if (arrayEquals(cursorPos, correctBox)) {

            addCorrect();
            genGuess();

        } else {

            addWrong();
            genGuess();

        }

    }    

    void updateTimer(float totalTime) {

        timer.text = "Seconds left: " + totalTime.ToString("F2");


    }

    string convertBoxString(int[] box) {

        if (box[1] == 1) {
            return box[0].ToString();
        } else {

            return (box_length + box[0]).ToString();

        }

    }

    void genGuess() {

        correctBox[0] = UnityEngine.Random.Range(1, box_length + 1);
        correctBox[1] = UnityEngine.Random.Range(1, box_height + 1);
        guess.text = convertBoxString(correctBox);

        guessbg.transform.Rotate(0, 0, 45);

    }

    void resetFunc() {

        reset.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);

        timerRunning = false;

        cursorPos[0] = box_length - box_lBound;
        cursorPos[1] = box_height;

        cursor.transform.position = origCursorPos;

        numCorrect = 0;
        numWrong = 0;

        correctCount.text = "";
        wrongCount.text = "";

        totalTime = 30f;

        txtbox.text = "Press enter to start";
        guess.text = "";
        timer.text = "";

       

    }

    void exitFunc() {

        Application.Quit();

    }

    void addCorrect() {

        correctSound.Play();
        numCorrect++;
        correctCount.text = "Correct: " + numCorrect.ToString();

    }

    void addWrong() {

        wrongSound.Play();
        numWrong++;
        wrongCount.text = "Wrong: " + numWrong.ToString();


    }

    bool arrayEquals(int[] array1, int[] array2) {

        for (int i = 0; i < array1.Length; i++) {

            if (array1[i] != array2[i]) {
                return false;
            }

        }

        return true;

    }

    int findMax(int[] nums) {

        if (Math.Abs(nums[0]) > Math.Abs(nums[1])) {
            return 0;
        }
        return 1;

    }
    

}


